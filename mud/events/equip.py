
from .event import Event2

class EquipOnEvent(Event2):
    NAME = "equip-on"

    def perform(self):
        if not self.object.has_prop("equipable"):
            self.fail()
            return self.inform("equip-on.failed")
        self.inform("equip-on")


class EquipOffEvent(Event2):
    NAME = "equip-off"

    def perform(self):
        if not self.object.has_prop("equipable"):
            self.fail()
            return self.inform("equip-off.failed")
        self.inform("equip-off")
