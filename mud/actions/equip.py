from .action import Action2
from mud.events import EquipOnEvent, EquipOffEvent

class EquipOnAction(Action2):
    EVENT = EquipOnEvent
    ACTION = "equip-on"
    RESOLVE_OBJECT = "resolve_for_use"

class EquipOffAction(Action2):
    EVENT = EquipOffEvent
    ACTION = "equip-off"
    RESOLVE_OBJECT = "resolve_for_use"
