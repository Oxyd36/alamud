from .effect import Effect2
from mud.events import EquipOnEvent, EquipOffEvent

class EquipOnEffect(Effect2):
    EVENT = EquipOnEvent

class EquipOffEffect(Effect2):
    EVENT = EquipOffEvent
